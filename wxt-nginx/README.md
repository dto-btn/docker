# Purpose
Drupal 8.8 with MySSC 3.0 site.

# Build
```bash
docker login registry.gitlab.com

docker build -t registry.gitlab.com/dto-btn/docker/wxt-nginx:1.17.8.1 .
docker push registry.gitlab.com/dto-btn/docker/wxt-nginx:1.17.8.1
```
