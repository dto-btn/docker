# Purpose
Creates a Node.js image with Oracle Instant Client database libraries included.  This makes it possible to run the node-oracledb module.

# Build
```bash
docker build -t node-oracledb:latest .
```