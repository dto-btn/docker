# Purpose
nginx docker image without the default nginx install and with the ability to set the Apache document root using the `APACHE_DOCUMENT_ROOT` environment variable.

# Build
```bash
docker login registry.gitlab.com

docker build -t registry.gitlab.com/dto-btn/docker/nginx:1.17.8 --build-arg NGINX_VERSION=1.17.8 .
docker push registry.gitlab.com/dto-btn/docker/nginx:1.17.8
```