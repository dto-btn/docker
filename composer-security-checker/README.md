# Purpose
Run [sensiolabs/security-checker](https://github.com/sensiolabs/security-checker) on a PHP Composer project's dependencies.

# Build
```bash
docker login registry.gitlab.com

docker build -t registry.gitlab.com/dto-btn/docker/composer-security-checker:7.2-cli --build-arg PHP_VERSION=7.2-cli .
docker push registry.gitlab.com/dto-btn/docker/composer-security-checker:7.2-cli
```