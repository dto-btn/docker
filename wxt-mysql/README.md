# Purpose
MySQL.

# Build
```bash
docker login registry.gitlab.com

docker build -t registry.gitlab.com/dto-btn/docker/wxt-mysql:5.7.29.1 .
docker push registry.gitlab.com/dto-btn/docker/wxt-mysql:5.7.29.1
```
