# Purpose
Drupal 8.8 with MySSC 3.0 site.

# Build
```bash
docker login registry.gitlab.com

docker build -t registry.gitlab.com/dto-btn/docker/myssc3-nginx:1.17.8 .
docker push registry.gitlab.com/dto-btn/docker/myssc3-nginx:1.17.8
```

# Using
Here is a sample docker-compose.yml file content to create a functional deployment using this image:

```yml
version: '3.7'

services:
    drupal:
        image: registry.gitlab.com/dto-btn/docker/myssc3-drupal:latest
        container_name: drupal-apache
        ports:
            - 8080:80
        environment:
            DRUPAL_DATABASE_DRIVER: mysql
            DRUPAL_DATABASE_HOST: drupal-mysql
            DRUPAL_DATABASE_PORT: 3306
            DRUPAL_DATABASE_NAME: myssc
            DRUPAL_DATABASE_USER: myssc
            DRUPAL_DATABASE_PASSWORD: myssc
        volumes:
            - /tmp/files:/var/www/html/web/sites/default/files

    mysql:
        image: mysql:5.7
        container_name: drupal-mysql
        ports:
            - "3306:3306"
        environment:
            MYSQL_RANDOM_ROOT_PASSWORD: 'yes'
            MYSQL_DATABASE: myssc
            MYSQL_USER: myssc
            MYSQL_PASSWORD: myssc
        volumes:
            - drupal-mysql:/var/lib/mysql

volumes:
    drupal-mysql:
```

Notice the use of DRUPAL_DATABASE_* environment variables to set the desired value in the container. We are also mounting a volume that will contain the site files.