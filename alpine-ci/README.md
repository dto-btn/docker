# Purpose
Alpine image with tools for CI pipelines.

# Build
```bash
docker login registry.gitlab.com

docker build -t registry.gitlab.com/dto-btn/docker/alpine-ci:latest .
docker push registry.gitlab.com/dto-btn/docker/alpine-ci:latest
```