# Purpose
Drupal 8.8 with wxt site.

# Build
```bash
docker login registry.gitlab.com

docker build -t registry.gitlab.com/dto-btn/docker/wxt-drupal-fpm:v3.0.8.1 .
docker push registry.gitlab.com/dto-btn/docker/wxt-drupal-fpm:v3.0.8.1
```
