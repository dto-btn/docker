# Purpose
Creates a Node.js image with Oracle Instant Client database libraries, docker and doccker-compose included.

# Build
```bash
docker build -t registry.gitlab.com/dto-btn/docker/node-oracledb-docker:latest .
docker push registry.gitlab.com/dto-btn/docker/node-oracledb-docker:latest
```