# Purpose
Ubuntu 18.04 with Ansible and a common SSH public/private key.

# Build
```bash
docker login registry.gitlab.com

docker build -t registry.gitlab.com/dto-btn/docker/ubuntu-ansible:18.04 .
docker push registry.gitlab.com/dto-btn/docker/ubuntu-ansible:18.04
```