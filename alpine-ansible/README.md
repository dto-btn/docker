# Purpose
Act as an Ansible controller to run playbooks against an environment.
1. The image expects to be built in the root of an Ansible project.
2. You must provide a private SSH key to the running Docker container that is in the `~/.ssh/authorized_keys` file of the hosts Ansible is running against.  This can be done at image build time or mounted as a volume.

# Build
The following will be built with no Ansible playbooks.  You will need to mount your playbooks at `/home/alpine`.
```bash
docker build -t alpine-ansible:latest .
```

This will build an image with Ansible playbooks included and specifies the version of Alpine to use:
```bash
docker build -t alpine-ansible:alpine-3.11.3 -f /path/to/alpine-ansible/Dockerfile --build-arg ALPINE_VERSION=3.11.3 /path/to/ansible-project
```