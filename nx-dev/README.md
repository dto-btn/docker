# Purpose
Creates a VScode devcontainer used for developing on an Nx.dev project with an Oracle DB.

# Build
```bash
docker build -t registry.gitlab.com/dto-btn/docker/nx-dev:latest .
docker push registry.gitlab.com/dto-btn/docker/nx-dev:latest
```