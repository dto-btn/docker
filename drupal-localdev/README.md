# Purpose
Drupal 8.8 with MySSC 3.0 site.

# Build
```bash
docker login registry.gitlab.com

docker build -t registry.gitlab.com/dto-btn/docker/drupal-localdev:v1.0.2 .
docker push registry.gitlab.com/dto-btn/docker/drupal-localdev:v1.0.2
```