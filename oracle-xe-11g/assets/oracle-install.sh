#!/bin/bash

# Install Oracle
cat /assets/oracle-xe_11.2.0-1.0_amd64.deba* > /assets/oracle-xe_11.2.0-1.0_amd64.deb &&
dpkg --install /assets/oracle-xe_11.2.0-1.0_amd64.deb &&

mv /assets/init.ora       /u01/app/oracle/product/11.2.0/xe/config/scripts
mv /assets/initXETemp.ora /u01/app/oracle/product/11.2.0/xe/config/scripts
mv /u01/app/oracle/product /u01/app/oracle-product

# Cleanup
apt-get clean && rm -rf /tmp/* /var/lib/apt/lists/* /var/tmp/*
rm -rf /assets
