# Purpose
Node with Gulp.

# Build
```bash
docker login registry.gitlab.com

docker build -t registry.gitlab.com/dto-btn/docker/node-gulp:10 --build-arg NODE_VERSION=10 .
docker push registry.gitlab.com/dto-btn/docker/node-gulp:10
```