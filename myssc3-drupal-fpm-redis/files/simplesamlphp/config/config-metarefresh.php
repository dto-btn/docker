# BEGIN ANSIBLE MANAGED BLOCK
<?php
   $config = [
     'sets' => [
       'adfs' => [
         'cron'      => ['hourly'],
         'sources'   => [
             [
                 'src' => 'https://fs.ciso.ssc-spc.gc.ca/federationmetadata/2007-06/federationmetadata.xml',
                 'validateFingerprint' => '729d1b0e4e3e5fe8053c9069cd59f4d9da24f7ec',
             ],
         ],
         'expireAfter'       => 60*60*24*4, // Maximum 4 days cache time.
         'outputDir'     => '/var/www/drupal/simplesamlphp/metadata/metarefresh-adfs-stack',
         'outputFormat' => 'flatfile',
       ],
       'adfs-sts' => [
         'cron'      => ['hourly'],
         'sources'   => [
             [
                 'src' => 'https://sts-sejs.prod.global.gc.ca/federationmetadata/2007-06/federationmetadata.xml',
                 'validateFingerprint' => '7871cc0682762e809841a9ca54ab5d7167ebf705',
             ],
         ],
         'expireAfter'       => 60*60*24*4, // Maximum 4 days cache time.
         'outputDir'     => '/var/www/drupal/simplesamlphp/metadata/metarefresh-adfs-sts',
         'outputFormat' => 'flatfile',
       ],
     ]
   ];
# END ANSIBLE MANAGED BLOCK