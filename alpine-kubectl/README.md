# Purpose
Perform Kubernetes deployments in a CI/CD pipeline using kubectl and kustomize.

# Build
```bash
docker build -t registry.gitlab.com/dto-btn/docker/alpine-kubectl:latest .
docker push registry.gitlab.com/dto-btn/docker/alpine-kubectl:latest
```
