# Purpose
Kubesec scan with kubectl included.

# Build
```bash
docker build -t registry.gitlab.com/dto-btn/docker/kubesec-kubectl:latest .
docker push registry.gitlab.com/dto-btn/docker/kubesec-kubectl:latest
```