# Purpose
Apache Solr 8.4.0 with MySSC 3.0 collection.

# Build
```bash
docker login registry.gitlab.com

docker build -t registry.gitlab.com/dto-btn/docker/myssc3-solr:8.4.1 .
docker push registry.gitlab.com/dto-btn/docker/myssc3-solr:8.4.1
```

# Using
Here is a sample kubernetes file content to create a functional deployment using this image:

```yml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: drupal-solr
  namespace: drupal8-demo
spec:
  replicas: 1
  selector:
    matchLabels:
      app: drupal-solr
  template:
    metadata:
      labels:
        app: drupal-solr
    spec:
      securityContext:
        fsGroup: 8983
      containers:
        - image: "registry.gitlab.com/dto-btn/docker/myssc3-solr:latest"
          name: drupal-solr
          ports:
            - containerPort: 8983
              name: solr
              protocol: TCP
          env:
            - name: "SOLR_JAVA_MEM"
              value: "-Xms2g -Xmx3g"
            - name: "SOLR_PORT"
              value: "8983"
            - name: "POD_HOSTNAME"
              valueFrom:
                fieldRef:
                  fieldPath: metadata.name
            - name: "SOLR_HOST"
              value: "$(POD_HOSTNAME).solr-headless"
            - name: "SOLR_LOG_LEVEL"
              value: "INFO"
          volumeMounts:
            - mountPath: /var/solr/data/myssc-solrconfig/data
              name: vol-solr
              subPath: data
      volumes:
        - name: vol-solr
          persistentVolumeClaim:
            claimName: drupal-pvc-solr
```

Notice the use of DRUPAL_DATABASE_* environment variables to set the desired value in the container. We are also mounting a volume that will contain the site files.