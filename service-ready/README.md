# Purpose
Query the Kubernetes API to check when a given service has active endpoints (pods that are ready).  This is meant to be used as an `initContainer` to control startup of dependent pods:

```yaml
initContainers:
- name: service-ready
  image: registry.gitlab.com/dto-btn/docker/service-ready:latest
  command: ['./service-ready.sh', 'your-namespace', 'your-service-name']
```

For this to work, you will need to grant the default service account for the namepace the **view** role:

```yaml
# rbac.yaml
---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: default-cluster
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: view
subjects:
- kind: ServiceAccount
  name: default
  namespace: your-namespace
```

```bash
kubectl create namespace your-namespace
kubectl apply -f rbac.yaml
```


# Build
```bash
docker build -t registry.gitlab.com/dto-btn/docker/service-ready:latest .
docker push registry.gitlab.com/dto-btn/docker/service-ready:latest
```