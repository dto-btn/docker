#!/bin/bash

# Use the Kubernetes API to check for a given service having pods that are ready.
# This works because service endpoints will not be listed until at least one pod is ready.
# Note: If curl returns 403 forbidden, it is because your service account does not have the `view` ClusterRole for the namespace.
NAMESPACE=$1
SERVICE=$2
API_URL="https://kubernetes.default.svc/api/v1/namespaces/${NAMESPACE}/endpoints/${SERVICE}"

function isServiceReady() {
  CACERT="/var/run/secrets/kubernetes.io/serviceaccount/ca.crt"
  TOKEN="$(cat /var/run/secrets/kubernetes.io/serviceaccount/token)"
  API_URL=$1

  RESULT=$(curl --cacert ${CACERT} --header "Authorization: Bearer ${TOKEN}" ${API_URL} | jq -r '.subsets[]?.addresses | length')
  if [[ -z $RESULT ]] || [[ "$RESULT" == "0" ]]; then
    return 1
  else
    return 0
  fi
}

until isServiceReady "$API_URL"; do
  echo -n "."
  sleep 10
done

echo "${SERVICE} ready!"