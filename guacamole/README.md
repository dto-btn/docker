# Purpose
Guacamole 1.1.0 with LDAP authentication support

# Build
```bash
docker login registry.gitlab.com

docker build -t registry.gitlab.com/dto-btn/docker/guacamole:1.1.0 -t registry.gitlab.com/dto-btn/docker/guacamole:latest .

docker push registry.gitlab.com/dto-btn/docker/guacamole
```
