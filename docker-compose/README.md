# Purpose
Docker-in-docker with Docker Compose.

# Build
```bash
docker login registry.gitlab.com

docker build -t registry.gitlab.com/dto-btn/docker/docker-compose:19.03.11-dind --build-arg DOCKER_VERSION=19.03.11-dind --build-arg COMPOSE_VERSION=1.25.5 .
docker push registry.gitlab.com/dto-btn/docker/docker-compose:19.03.11-dind
```