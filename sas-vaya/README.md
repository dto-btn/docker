# Purpose
Drupal docker image without the default Drupal install and with the ability to set the Apache document root using the `APACHE_DOCUMENT_ROOT` environment variable.

# Build
```bash
docker login registry.gitlab.com

docker build -t registry.gitlab.com/dto-btn/docker/drupal:8.8.1-apache --build-arg DRUPAL_VERSION=8.8.1-apache .
docker push registry.gitlab.com/dto-btn/docker/drupal:8.8.1-apache
```